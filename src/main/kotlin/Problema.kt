import com.fasterxml.jackson.databind.annotation.JsonSerialize
import kotlinx.serialization.Serializable

@JsonSerialize
class Problema(
    val id: Int,
    val enunciat: String,
    val jocDeProvesPublic: JocDeProves,
    val jocDeProvesPrivat: JocDeProves,
    var resolt: Boolean = false,
    var intents: Int,
    var llistaIntents: MutableList<Intent>) {

    fun mostrarProblema() {
        println(enunciat)
        println("\nEntrada: ${jocDeProvesPublic.input}")
        println("\nSortida: ${jocDeProvesPublic.output}")
    }

    fun mostrarNombreDeIntents() {
        println("\nNombre dels intents: $intents")
    }

    fun mostrarElsIntentsFets() {
        println("\nIntents: $llistaIntents")
    }

    fun afegirIntent(intent: Intent) {
        llistaIntents.add(intent)
    }

    fun sumarUnIntent() {
        intents += 1
    }

    fun comprovarResposta(resposta: String): Boolean {
        return resposta.uppercase() == jocDeProvesPrivat.output.uppercase()
    }
}