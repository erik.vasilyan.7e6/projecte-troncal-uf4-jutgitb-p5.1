import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize
data class Intent(val resposta: String, val timestamp: String)