import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize
data class JocDeProves (val input: String, val output: String)