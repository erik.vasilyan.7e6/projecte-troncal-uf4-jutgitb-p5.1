import org.postgresql.util.PSQLException
import java.sql.Connection
import java.sql.DriverManager
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.system.exitProcess

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-06
    * TITLE: Jutge de codi
 */

const val defaultColor = "\u001B[0m"
const val red = "\u001B[31m"
const val green = "\u001B[32m"
private var llistaProblemes = mutableListOf<Problema>()

private const val url = "jdbc:postgresql://localhost:5432/jutge"
private const val username = "admin"
private const val password = "admin123"

private lateinit var connection: Connection

fun main() {
    val scanner = Scanner(System.`in`)

    try {
        connection = DriverManager.getConnection(url, username, password)

        llegirProblemesDeBd()
        print(">>> Jutge <<<\n")
        repeat(14) { print("-") }
        llistaProblemes.shuffle()
        mostrarMenuPrincipal(scanner)

    } catch (e: PSQLException) {
        println("Error: ${e.message}")
    }
}

fun llegirProblemesDeBd() {

    try {
        var enunciatProblema: String
        var idProblema: Int
        var problemaResolt: Boolean
        var intents: Int
        var jocDeProvesPublic = JocDeProves("", "")
        var jocDeProvesPrivat = JocDeProves("", "")

        val querySelectTotsElsProblemes = "SELECT * FROM problema"
        val querySelectElJocDeProvesPublicById = "SELECT * FROM jocdeprovespublic WHERE id_problema = ?"
        val querySelectElJocDeProvesPrivatById = "SELECT * FROM jocdeprovesprivat WHERE id_problema = ?"
        val querySelectTotsIntentsFetsDelProblemaById = "SELECT * FROM intent WHERE id_problema = ?"

        connection.prepareStatement(querySelectTotsElsProblemes).use { statementProblema ->
            val resultProblema = statementProblema.executeQuery()
            while (resultProblema.next()) {
                enunciatProblema = resultProblema.getString("enunciat")
                idProblema = resultProblema.getInt("id_problema")
                problemaResolt = resultProblema.getBoolean("resolt")
                intents = resultProblema.getInt("intents")

                connection.prepareStatement(querySelectElJocDeProvesPublicById).use { statementJocDeProvesPublic ->
                    statementJocDeProvesPublic.setInt(1, idProblema)
                    val resultJocDeProvesPublic = statementJocDeProvesPublic.executeQuery()
                    while (resultJocDeProvesPublic.next()) {
                        val jocDeProvesPublicInput = resultJocDeProvesPublic.getString("input")
                        val jocDeProvesPublicOutput = resultJocDeProvesPublic.getString("output")
                        jocDeProvesPublic = JocDeProves(jocDeProvesPublicInput, jocDeProvesPublicOutput)
                    }
                }

                connection.prepareStatement(querySelectElJocDeProvesPrivatById).use { statementJocDeProvesPrivat ->
                    statementJocDeProvesPrivat.setInt(1, idProblema)
                    val resultJocDeProvesPrivat = statementJocDeProvesPrivat.executeQuery()
                    while (resultJocDeProvesPrivat.next()) {
                        val jocDeProvesPrivatInput = resultJocDeProvesPrivat.getString("input")
                        val jocDeProvesPrivatOutput = resultJocDeProvesPrivat.getString("output")
                        jocDeProvesPrivat = JocDeProves(jocDeProvesPrivatInput, jocDeProvesPrivatOutput)
                    }
                }

                connection.prepareStatement(querySelectTotsIntentsFetsDelProblemaById).use { statementIntent ->
                    statementIntent.setInt(1, idProblema)
                    val resultIntent = statementIntent.executeQuery()
                    val llistaIntents = mutableListOf<Intent>()
                    while (resultIntent.next()) {
                        val intentRespota = resultIntent.getString("resposta")
                        val intentTimestamp = resultIntent.getString("timestamp")
                        llistaIntents.add(Intent(intentRespota, intentTimestamp))
                    }

                    val problema = Problema(idProblema, enunciatProblema, jocDeProvesPublic, jocDeProvesPrivat, problemaResolt, intents, llistaIntents)
                    llistaProblemes.add(problema)
                }
            }
        }

    } catch (e: PSQLException) {
        println("Error: ${e.message}")
    }
}

fun afegirProblemaADb(problema: Problema) {

    val sqlTaulaProblema = "INSERT INTO problema (enunciat, resolt, intents) VALUES (?, ?, ?)"
    val sqlTaulaJocDeProvesPublic = "INSERT INTO jocdeprovespublic (id_problema, input, output) VALUES (?, ?, ?)"
    val sqlTaulaJocDeProvesPrivat = "INSERT INTO jocdeprovesprivat (id_problema, input, output) VALUES (?, ?, ?)"

    try {
        val taulaProblemaStatement = connection.prepareStatement(sqlTaulaProblema)
        taulaProblemaStatement.setString(1, problema.enunciat)
        taulaProblemaStatement.setBoolean(2, false)
        taulaProblemaStatement.setInt(3, 0)
        taulaProblemaStatement.executeUpdate()

        val taulaJocDeProvesPublicStatement = connection.prepareStatement(sqlTaulaJocDeProvesPublic)
        taulaJocDeProvesPublicStatement.setInt(1, problema.id)
        taulaJocDeProvesPublicStatement.setString(2, problema.jocDeProvesPublic.input)
        taulaJocDeProvesPublicStatement.setString(3, problema.jocDeProvesPublic.output)
        taulaJocDeProvesPublicStatement.executeUpdate()

        val taulaJocDeProvesPrivatStatement = connection.prepareStatement(sqlTaulaJocDeProvesPrivat)
        taulaJocDeProvesPrivatStatement.setInt(1, problema.id)
        taulaJocDeProvesPrivatStatement.setString(2, problema.jocDeProvesPrivat.input)
        taulaJocDeProvesPrivatStatement.setString(3, problema.jocDeProvesPrivat.output)
        taulaJocDeProvesPrivatStatement.executeUpdate()

    } catch (e: PSQLException) {
        println("Error: ${e.message}")
    }
}

fun modificarProblemaDeDb(problema: Problema) {

    try {
        // Sentencia para modificar un problema
        val queryModificarProblema = "UPDATE problema SET enunciat = ?, resolt = ?, intents = ? WHERE id_problema = ?"
        val preparedStatementProblema = connection.prepareStatement(queryModificarProblema)
        preparedStatementProblema.setString(1, problema.enunciat)
        preparedStatementProblema.setBoolean(2, problema.resolt)
        preparedStatementProblema.setInt(3, problema.intents)
        preparedStatementProblema.setInt(4, problema.id)
        // Ejecuta el statement hecho para modificar el problema
        preparedStatementProblema.executeUpdate()

        // Sentencia para modificar Joc De Proves Public
        val queryModificarJocDeProvesPublic = "UPDATE jocdeprovespublic SET input = ?, output = ? WHERE id_problema = ?"
        val preparedStatementJocDeProvesPublic = connection.prepareStatement(queryModificarJocDeProvesPublic)
        preparedStatementJocDeProvesPublic.setString(1, problema.jocDeProvesPublic.input)
        preparedStatementJocDeProvesPublic.setString(2, problema.jocDeProvesPublic.output)
        preparedStatementJocDeProvesPublic.setInt(3, problema.id)
        // Ejecuta el statement hecho para modificar el Joc De Proves Public
        preparedStatementJocDeProvesPublic.executeUpdate()

        // Sentencia para modificar Joc De Proves Privat
        val queryModificarJocDeProvesPrivat = "UPDATE jocdeprovesprivat SET input = ?, output = ? WHERE id_problema = ?"
        val preparedStatementJocDeProvesPrivat = connection.prepareStatement(queryModificarJocDeProvesPrivat)
        preparedStatementJocDeProvesPrivat.setString(1, problema.jocDeProvesPrivat.input)
        preparedStatementJocDeProvesPrivat.setString(2, problema.jocDeProvesPrivat.output)
        preparedStatementJocDeProvesPrivat.setInt(3, problema.id)
        // Ejecuta el statement hecho para modificar el Joc De Proves Public
        preparedStatementJocDeProvesPrivat.executeUpdate()

    } catch (e: PSQLException) {
        println("Error: ${e.message}")
    }
}

fun afegirIntentADb(intent: Intent, idProblema: Int) {
    val queryInsertIntent = "INSERT INTO intent (id_problema, resposta, timestamp) VALUES (?, ?, ?)"

    try {
        val preparedStatementIntent = connection.prepareStatement(queryInsertIntent)
        preparedStatementIntent.setInt(1, idProblema)
        preparedStatementIntent.setString(2, intent.resposta)
        preparedStatementIntent.setString(3, intent.timestamp)
        preparedStatementIntent.executeUpdate()

    } catch (e: PSQLException) {
        println("Error: ${e.message}")
    }
}

fun deleteProblemaFromDb(idProblema: Int) {
    try {
        // Sentencia para eliminar el joc de proves public del problema de base de datos
        val queryDeleteJocDeProvesPublic = "DELETE FROM jocdeprovespublic WHERE id_problema = ?"
        val preparedStatementJocDeProvesPublic = connection.prepareStatement(queryDeleteJocDeProvesPublic)
        preparedStatementJocDeProvesPublic.setInt(1, idProblema)
        // Ejecuta el statement hecho para la eliminación
        preparedStatementJocDeProvesPublic.execute()

        // Sentencia para eliminar el joc de proves privat del problema de base de datos
        val queryDeleteJocDeProvesPrivat = "DELETE FROM jocdeprovesprivat WHERE id_problema = ?"
        val preparedStatementJocDeProvesPrivat = connection.prepareStatement(queryDeleteJocDeProvesPrivat)
        preparedStatementJocDeProvesPrivat.setInt(1, idProblema)
        // Ejecuta el statement hecho para la eliminación
        preparedStatementJocDeProvesPrivat.execute()

        // Sentencia para eliminar los intentos del problema de base de datos
        val queryDeleteIntent = "DELETE FROM intent WHERE id_problema = ?"
        val preparedStatementIntent = connection.prepareStatement(queryDeleteIntent)
        preparedStatementIntent.setInt(1, idProblema)
        // Ejecuta el statement hecho para la eliminación
        preparedStatementIntent.execute()

        // Sentencia para eliminar el problema de base de datos
        val queryDeleteProblema = "DELETE FROM problema WHERE id_problema = ?"
        val preparedStatementProblema = connection.prepareStatement(queryDeleteProblema)
        preparedStatementProblema.setInt(1, idProblema)
        // Ejecuta el statement hecho para la eliminación
        preparedStatementProblema.execute()

        llistaProblemes.removeIf { it.id == idProblema }

        println("\n${green}El problema s'ha eliminat correctament$defaultColor")
    } catch (e: PSQLException) {
        println("\n${red}El problema no s'ha eliminat, error: ${e.message}$defaultColor")
    }
}

fun mostrarMenuAlumnat(scanner: Scanner) {
    println("\nBenvingut a l'area d'Alumnat\n")

    println("1. Seguir amb l’itinerari d’aprenentatge")
    println("2. Llista problemes")
    println("3. Consultar històric de problemes resolts")
    println("4. Ajuda")
    println("5. Sortir")

    var opcio: Int
    do {
        print("\nOpció: ")
        opcio = scanner.nextInt()
        if (opcio !in 1..5) println("$opcio no és una opció valida!")

    } while (opcio !in 1..5)

    when (opcio) {
        1 -> { seguirAmbItinerariAprenentatge(scanner) }
        2 -> { llistaProblemes(scanner) }
        3 -> { consultarHistoricDeProblemesResolts(scanner) }
        4 -> { mostrarAjuda(scanner) }
        5 -> { exitProcess(0) }
    }
}

fun mostrarMenuPrincipal(scanner: Scanner) {
    println("\n\n1. Soc alumne")
    println("2. Soc professor")

    do {
        print("\nOpció: ")
        val opcio = scanner.next()
        when (opcio) {
            "1" -> { mostrarMenuAlumnat(scanner) }
            "2" -> { inicioProfessorat(scanner) }
        }
    } while (opcio != "1" && opcio != "2")
}

fun inicioProfessorat(scanner: Scanner) {
    var intents = 3

    println("\nBenvingut a l'area de Professorat")

    do {
        print("\nIntrodueix la contrasenya (0 per sortir): ")
        val contrasenya = scanner.next()

        if (contrasenya == "0") exitProcess(0)

        if (contrasenya == "professorat") {
            println("\n${green}Contrasenya correcta!$defaultColor")
            mostrarMenuProfessorat(scanner)
        }
        else {
            intents--
            println("\n${red}Contrasenya incorrecta!$defaultColor")
            println("Intents restes: $intents")
        }

    } while (contrasenya != "professorat" && intents != 0)

    if (intents == 0) exitProcess(0)
}

fun mostrarMenuProfessorat(scanner: Scanner) {
    println("\nBenvingut a l'area de Professorat\n")

    println("1. Afegir nous problemes")
    println("2. Treure un report de la feina de l’alumne")
    println("3. Modificar un problema")
    println("4. Esborrar un problema")
    println("5. Sortir")

    var opcio: Int
    do {
        print("\nOpció: ")
        opcio = scanner.nextInt()

        if (opcio !in 1..5) println("$opcio no és una opció valida!")

    } while (opcio !in 1..5)
    println("")
    when (opcio) {
        1 -> { afegirNousProblemes(scanner) }
        2 -> {
            treureReport()
            mostrarMenuProfessorat(scanner)
        }
        3 -> {
            mostrarLlistaDeProblemes()
            val opcioUsuari = preguntarPerOpcio(scanner)
            val problemaPerModificar = llistaProblemes.find { it.id == opcioUsuari }!!
            val problemaModificat = modificarProblema(scanner, problemaPerModificar)
            modificarProblemaDeDb(problemaModificat)
            mostrarMenuProfessorat(scanner)
        }
        4 -> {
            mostrarLlistaDeProblemes()
            val opcioUsuari = preguntarPerOpcio(scanner)
            deleteProblemaFromDb(opcioUsuari)
            mostrarMenuProfessorat(scanner)
        }
        5 -> { exitProcess(0) }
    }
}

fun modificarProblema(scanner: Scanner, problemaPerModificar: Problema): Problema {
    var enunciatProblema = problemaPerModificar.enunciat
    var jocDeProvesPublicEntrada = problemaPerModificar.jocDeProvesPublic.input
    var jocDeProvesPublicSortida = problemaPerModificar.jocDeProvesPublic.output
    var jocDeProvesPrivatEntrada = problemaPerModificar.jocDeProvesPrivat.input
    var jocDeProvesPrivatSortida = problemaPerModificar.jocDeProvesPrivat.output

    println("\n1. Modificar l'enunciat del problema")
    println("2. Modificar el joc de proves public")
    println("3. Modificar el joc de proves privat")

    when(preguntarPerOpcio(scanner)) {
        1 -> {
            print("\nEnunciat: ")
            scanner.nextLine()
            enunciatProblema = scanner.nextLine()
        }
        2 -> {
            print("\nJoc de proves public (entrada): ")
            scanner.nextLine()
            jocDeProvesPublicEntrada = scanner.nextLine()

            print("\nJoc de proves public (sortida): ")
            jocDeProvesPublicSortida = scanner.nextLine()
        }
        3 -> {
            print("\nJoc de proves privat (entrada): ")
            scanner.nextLine()
            jocDeProvesPrivatEntrada = scanner.nextLine()

            print("\nJoc de proves privat (sortida): ")
            jocDeProvesPrivatSortida = scanner.nextLine()
        }
    }

    println("\n${green}El problema s'ha modificat correctament$defaultColor")

    val problemaModificat = Problema(problemaPerModificar.id, enunciatProblema, JocDeProves(jocDeProvesPublicEntrada, jocDeProvesPublicSortida), JocDeProves(jocDeProvesPrivatEntrada, jocDeProvesPrivatSortida), problemaPerModificar.resolt, problemaPerModificar.intents, problemaPerModificar.llistaIntents)
    llistaProblemes[llistaProblemes.indexOf(problemaPerModificar)] = problemaModificat

    return problemaModificat
}

fun treureReport() {

    var puntuacio = 0.0

    println("Problemes resolts: ")
    for (problema in llistaProblemes) {
        if (problema.resolt) {
            var puntuacioDeProblema = 0.0
            println("   ${problema.enunciat}")
            println("   - Intents: ${problema.intents}")
            puntuacioDeProblema += 0.5
            if (problema.intents > 1) {
                puntuacioDeProblema -= (problema.intents - 1) * 0.1
            }
            if (puntuacioDeProblema < 0.0) puntuacioDeProblema = 0.0
            println("   - Puntuació de problema: $puntuacioDeProblema/0.5")
            puntuacio += puntuacioDeProblema
        }
    }
    repeat(25) { print("-") }
    println("\nPuntuació final: $puntuacio")
}

fun afegirNousProblemes(scanner: Scanner) {
    print("Quants problemes vols afegir? : ")
    val nombreDeProblemsPerAfegir = scanner.nextInt()

    println("\nIntrodueix els camps necessaris a contiunació...\n")

    for (i in 1..nombreDeProblemsPerAfegir) {
        print("Enunciat: ")
        if (i == 1) scanner.nextLine()
        val enunciatProblema = scanner.nextLine()

        print("\nJoc de proves public (entrada): ")
        val jocDeProvesPublicEntrada = scanner.nextLine()

        print("Joc de proves public (sortida): ")
        val jocDeProvesPublicSortida = scanner.nextLine()

        print("Joc de proves privat (entrada): ")
        val jocDeProvesPrivatEntrada = scanner.nextLine()

        print("Joc de proves privat (sortida): ")
        val jocDeProvesPrivatSortida = scanner.nextLine()

        val problemaPerAfegir = Problema(findLastId()+1, enunciatProblema, JocDeProves(jocDeProvesPublicEntrada, jocDeProvesPublicSortida), JocDeProves(jocDeProvesPrivatEntrada, jocDeProvesPrivatSortida), false, 0, mutableListOf())
        llistaProblemes.add(problemaPerAfegir)
        afegirProblemaADb(problemaPerAfegir)
        println("\n${green}Problema afegit amb èxit$defaultColor")

    }
    mostrarMenuProfessorat(scanner)
}

fun findLastId(): Int {
    var idProblema = 0

    try {
        // Sentencia para encontrar el id del problema añadido
        val querySelectLastValue = "SELECT last_value FROM problema_id_problema_seq"
        val preparedStatementLastValue = connection.prepareStatement(querySelectLastValue)
        val resultLastValue = preparedStatementLastValue.executeQuery()
        while (resultLastValue.next()) {
            idProblema = resultLastValue.getInt("last_value")
        }

    } catch (e: PSQLException) {
        println("Error: ${e.message}")
    }

    return idProblema
}

fun seguirAmbItinerariAprenentatge(scanner: Scanner) {
    println("")
    var problemesResolts = 0
    for (problema in llistaProblemes) {
        if (!problema.resolt) {
            resoldreProblema(problema, scanner)
            repeat(45) { print("-") }
            println("\n")
        }
        else problemesResolts++
    }

    if (problemesResolts == llistaProblemes.size) println("Has resolt tots els problemes! Enhorabona!")
    mostrarMenuAlumnat(scanner)
}

fun llistaProblemes(scanner: Scanner) {
    println("")
    for (problema in llistaProblemes.sortedBy { it.id }) {
        println("${problema.id}. ${problema.enunciat}")
    }

    var opcio: Int
    do {
        print("\nIntrodueix el nombre del problema que vols resoldre (0 per sortir): ")
        opcio = scanner.nextInt()
        if (opcio == 0) mostrarMenuAlumnat(scanner)
        if (opcio !in 1..llistaProblemes.size) println("$opcio no és una opció valida!")

    } while (opcio !in 1..llistaProblemes.size)

    val problema = llistaProblemes.find { it.id == opcio }!!

    resoldreProblema(problema, scanner)
    mostrarMenuAlumnat(scanner)
}

fun mostrarLlistaDeProblemes() {
    for (problema in llistaProblemes.sortedBy { it.id }) {
        println("${problema.id}. ${problema.enunciat}")
    }
}

fun consultarHistoricDeProblemesResolts(scanner: Scanner) {
    println("")
    for (problema in llistaProblemes.sortedBy { it.id }) {
        println("${problema.id}. ${problema.enunciat}")
        println("   - Entrada: ${problema.jocDeProvesPrivat.input}")
        println("   - Intents: ${problema.llistaIntents}")
        println("   - Resolt: ${problema.resolt}")
    }

    mostrarMenuAlumnat(scanner)
}

fun mostrarAjuda(scanner: Scanner) {
    println("\nBenvingut al sistema de jutge de codi de l'Institut Tecnològic de Barcelona.")

    println("\nAquesta eina està dissenyada per ajudar-te en el teu aprenentatge de programació.")
    println("Aquest programa t'ofereix diferents funcionalitats que et permetran seguir un itinerari d'aprenentatge,\nAccedir a una llista de problemes i consultar l'historial de problemes resolts.")
    println("Per accedir a aquestes funcionalitats, pots utilitzar el menú que s'obre en iniciar el programa.")

    println("\nSi ets un professor, també tens la possibilitat d'accedir a una opció exclusiva per gestionar l'eina des del teu rol.\nPer accedir a aquesta opció, necessitaràs la contrasenya que només tu coneixes.\nA través d'aquesta opció, podràs afegir nous problemes,\nObtenir un report de la feina dels alumnes,\nAssignar puntuacions en funció dels problemes resolts i descomptar punts per intents.")
    println("")

    mostrarMenuAlumnat(scanner)
}

fun preguntarPerOpcio(scanner: Scanner): Int {
    print("\nOpció: ")
    return scanner.nextInt()
}

fun resoldreProblema(problema: Problema, scanner: Scanner) {
    problema.mostrarProblema()

    print("\nVols resoldre aquest problema? [SI / NO / 0 (sortir)]: ")
    var opcio = scanner.next()

    if (opcio == "0") mostrarMenuAlumnat(scanner)

    while (opcio.uppercase() == "SI") {

        if (problema.intents > 0) problema.mostrarNombreDeIntents()
        if (problema.llistaIntents.isNotEmpty()) problema.mostrarElsIntentsFets()

        println("\nEntrada: ${problema.jocDeProvesPrivat.input}")
        print("\nSortida: ")
        val respostaUsuari = scanner.next()

        val now = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        val timestamp = now.format(formatter)

        val intent = Intent(respostaUsuari, timestamp)
        problema.sumarUnIntent()
        problema.afegirIntent(intent)
        afegirIntentADb(intent, problema.id)

        val respostaCorrecta = problema.comprovarResposta(respostaUsuari)

        if (respostaCorrecta) {
            println("\n${green}Resposta correcta!$defaultColor")
            problema.resolt = true
            opcio = "NO"
        }
        else {
            println("\n${red}Resposta incorrecta!$defaultColor")
            print("\nVols intentar una vegada més? [SI / NO]: ")
            opcio = scanner.next().uppercase()
        }

        modificarProblemaDeDb(problema)
    }
}