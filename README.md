### Instal·lació
---

```bash
$ git clone https://gitlab.com/erik.vasilyan.7e6/projecte-troncal-uf4-jutgitb-p5.1.git
```

També s'ha descarregar el servidor PostgreSQL des del fitxer zip i exportar-lo.


### Configuració de PostgreSQL
---

Primer de tot obriu el servidor PostgreSQL en el terminal.

El servidor ja està en funcionament, si voleu comprovar la base de dades de Jutge podeu fer els següents passos:

$ psql -U admin -d jutge

### Ús
---

Per começar ja podeu executar el Main.kt i utilizar el programa.
